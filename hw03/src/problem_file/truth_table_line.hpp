#ifndef TRUTH_TABLE_LINE__HPP
#define TRUTH_TABLE_LINE__HPP

#include "ast/symbol.hpp"
#include <compare>
#include <map>
#include <stdint.h>

namespace problem_file {
class truth_table_line {
	friend class problem_file;
	friend bool operator==(const truth_table_line &, const truth_table_line &);
	friend bool operator!=(const truth_table_line &, const truth_table_line &);
	/**
	 * @brief
	 *
	 * @return std::partial_ordering::unordered if the two have different numbers
	 * of variables or different variables if they have the same count.
	 * @return std::partial_ordering::equivalent if the have equivalent symbols
	 * and values assigned to them.
	 * This does not necessarily mean operator== will return true.
	 * @return std::partial_ordering::equivalent if the have equivalent symbols
	 * and values assigned to them.
	 * This does not necessarily mean operator== will return true.
	 */
	friend std::partial_ordering operator<=>(const truth_table_line &,
	                                         const truth_table_line &);

public:
	typedef std::map<ast::symbol, bool> map_type;

protected:
	map_type _symbols;

	truth_table_line(map_type &symbols, bool result);
	truth_table_line(map_type &&symbols, bool result);

public:
	bool result;
	/**
	 * @brief Get the number of distinct variables in the problem
	 */
	uint_fast32_t varcount() const;
	// For now you just can't set the count.
	// /**
	//  * @brief Set the number of distinct variables in the problem
	//  */
	// void varcount(uint_fast32_t count);

	/**
	 * @brief Get the symbol mapping in the truth table
	 *
	 * @return const map_type
	 */
	const map_type &symbols() const;
	/**
	 * @brief Set the symbol to have the specified value
	 *
	 * @param symbol The symbol to change
	 * @param value The value to change it to
	 *
	 * @throw  std::out_of_range  If no such data is present.
	 */
	void set_symbol(const ast::symbol &symbol, bool value);
};
bool operator==(const truth_table_line &lhs, const truth_table_line &rhs);
bool operator!=(const truth_table_line &lhs, const truth_table_line &rhs);
std::partial_ordering operator<=>(const truth_table_line &lhs,
                                  const truth_table_line &rhs);
} // namespace problem_file

#endif
