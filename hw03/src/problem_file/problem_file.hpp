#ifndef PROBLEM_FILE__HPP
#define PROBLEM_FILE__HPP

#include "ast/symbol.hpp"
#include "truth_table_line.hpp"
#include <set>
#include <stdint.h>
#include <string>

namespace problem_file {
class problem_file {
public:
	typedef std::set<truth_table_line> truth_table_type;

protected:
	/**
	 * @brief Symbols in the truth table
	 */
	std::set<ast::symbol> _symbols;
	/**
	 * @brief Number of variables in the problem
	 */
	uint_fast16_t _varcount;
	/**
	 * @brief "Number of random constants to be provided in the primitive set."
	 * I'm not sure what that means either.
	 */
	uint_fast32_t _nrand;
	/**
	 * @brief Minimum value possible for a parameter
	 */
	double _minrand;
	/**
	 * @brief Maximum value possible for a parameter
	 */
	double _maxrand;
	// /**
	//  * @brief Number of cases in the truth table
	//  */
	// uint_fast32_t _nfitcases;
	/**
	 * @brief Truth table lines
	 */
	truth_table_type _truth_table;

public:
	/**
	 * @brief Get the number of distinct variables in the problem
	 */
	uint_fast16_t varcount() const;
	// For now, you just can't change the number of variables.
	// TODO Make it so you can change the number of variables
	// /**
	//  * @brief Set the number of distinct variables in the problem
	//  */
	// void varcount(uint_fast16_t count);

	/**
	 * @brief Get the symbol mapping in the truth table
	 *
	 * @return truth_table_type
	 */
	// truth_table_type &truth_table() noexcept;
	/**
	 * @brief Get the symbol mapping in the truth table, as const.
	 *
	 * @return const truth_table_type
	 */
	const truth_table_type &truth_table() const noexcept;
	/**
	 * @brief Get the symbols that appear in the truth table
	 *
	 * @return std::set<ast::symbol>
	 */
	const std::set<ast::symbol> &symbols() const noexcept;

	/**
	 * @brief Parses filepath
	 *
	 * @param filepath path to file to parse
	 * @return problem_file&& the problem file as an object
	 * @throw read_error An error was encoutered
	 */
	static problem_file load_file(std::string &filepath);
	/**
	 * @brief Parses filepath
	 *
	 * @param filepath path to file to parse
	 * @return problem_file&& the problem file as an object
	 * @throw read_error An error was encoutered
	 */
	static problem_file load_file(std::string &&filepath);
};
} // namespace problem_file

#endif
