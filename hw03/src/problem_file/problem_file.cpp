#include "problem_file.hpp"
#include "ast/symbol.hpp"
#include "read_error.hpp"
#include "truth_table_line.hpp"
#include <fstream>
#include <sstream>
#include <string>
uint_fast16_t problem_file::problem_file::varcount() const {
	return this->_varcount;
}
// problem_file::problem_file::truth_table_type &
// problem_file::problem_file::truth_table() {
// 	return this->_truth_table;
// }
const problem_file::problem_file::truth_table_type &
problem_file::problem_file::truth_table() const noexcept {
	return this->_truth_table;
}

const std::set<ast::symbol> &
problem_file::problem_file::symbols() const noexcept {
	return this->_symbols;
}

/**
 * @brief Tries to convert a token to a boolean value
 *
 * @param token Any of true, false, t, f, on, off, yes, no, y, n, 1, 0
 * @return Result of trying to convert
 * @throw problem_file::read_error Conversion error
 */
static bool bool_from_str(std::string &token) {
	if (token.size() == 1 && (token == "t" || token == "y" || token == "1")
	    || token == "true" || token == "on" || token == "yes") {
		//true, t, on, yes, y, 1
		return true;
	}
	else if (token.size() == 1 && (token == "f" || token == "n" || token == "0")
	         || token == "false" || token == "off" || token == "no") {
		//false, f, off, no, n, 0
		return false;
	}
	else {
		std::ostringstream msg;
		msg << "invalid value \'" << token << "\' encountered.";
		throw problem_file::read_error(msg.str());
	}
}

/**
 * File format:
 * First line:
 *   - Number of variables to use
 *   - Space
 *   - Random number
 *   - Space
 *   - Minimum random number
 *   - Space
 *   - Maximum random number
 *   - Space
 *   - Number of cases
 *
 * Remaining lines, count equal to number of cases:
 *   - Variables separated by spaces
 *   - Space
 *   - Result
 *
 * May optionally end with a newline
 *
 * Variables may be any of:
 * true, false, t, f, on, off, yes, no, y, n, 1, 0
 */
problem_file::problem_file
problem_file::problem_file::load_file(std::string &filepath) {
	std::ifstream file(filepath);
	if (!file.good()) {
		throw std::invalid_argument("file is not good for reading");
	}
	std::string token;
	std::getline(file, token);
	// // Windows and its UTF-8 with BOM sucks. BOM is either FFFE or FEFF.
	// // Since the file *should* be UTF-8, we can just read two characters and
	// // throw them away if they're either of those two.
	// if () {}

	/// The result that will be returned from this function
	problem_file result;

	/// The current line being parsed
	std::istringstream sstr(token);
	sstr >> std::skipws;
	/// Number of lines in our truth table
	uint_fast32_t nfitcases;
	sstr >> result._varcount >> result._nrand >> result._minrand
	  >> result._maxrand >> nfitcases;
	if (sstr.fail()) {
		throw read_error("unable to read line 1 of file");
	}
	else if (result._varcount == 0) {
		throw read_error("Cannot have zero variables");
	}
	else if (nfitcases == 0) {
		throw read_error("Cannot have zero fitness cases");
	}

	/// AST symbols, with some names
	result._symbols = ast::generate_symbols(result._varcount);

	for (size_t i = 0; i < nfitcases; ++i) {
		std::getline(file, token);
		sstr.str(token);
		/// Variables and result
		bool variables[result._varcount + 1];
		try {
			for (size_t j = 0; j < result._varcount + 1; ++j) {
				sstr >> token;
				if (token.size() == 0) {
					throw read_error("insufficient tokens.");
				}
				variables[j] = bool_from_str(token);
			}
			auto *var = variables;
			auto symbol = result._symbols.begin(), symbol_end = result._symbols.end();
			// Create map from symbol to value
			truth_table_line::map_type table_line;
			for (; symbol != symbol_end; ++var, ++symbol) {
				table_line[*symbol] = *var;
			}
			// Check if the line was already listed
			// std::set::insert returns a pair, with the second value being false
			// if the value was not inserted
			if (!(result._truth_table
			        .insert(truth_table_line(std::move(table_line),
			                                 variables[result._varcount]))
			        .second)) {
				throw read_error("values already listed.");
			}
		}
		catch (read_error &exc) {
			std::ostringstream msg(exc.message);
			msg << " \'" << filepath << "\':" << i + 2;
			exc.message = msg.str();
			throw exc;
		}
	}

	// Check we reached the end of the file and there aren't extraneous lines
	std::getline(file, token);
	// Need to account for possible newline at the end of the file
	if (token.size() != 0 || !file.eof()) {
		std::ostringstream msg;
		msg << "unexpected line, expected " << nfitcases
		    << " number of cases but got more.";
		msg << " \'" << filepath << "\':" << nfitcases + 2;
		throw read_error(msg.str());
	}

	return std::move(result);
}

problem_file::problem_file
problem_file::problem_file::load_file(std::string &&filepath) {
	return problem_file::problem_file::load_file(filepath);
}
