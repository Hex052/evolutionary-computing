#include "read_error.hpp"

problem_file::read_error::read_error(std::string &message) : message(message) {
}
problem_file::read_error::read_error(std::string &&message) : message(message) {
}

const char *problem_file::read_error::what() const noexcept {
	return this->message.c_str();
}
