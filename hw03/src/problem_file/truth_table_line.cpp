#include "truth_table_line.hpp"

problem_file::truth_table_line::truth_table_line(
  std::map<ast::symbol, bool> &symbols, bool result)
    : _symbols(symbols), result(result) {
}
problem_file::truth_table_line::truth_table_line(
  std::map<ast::symbol, bool> &&symbols, bool result)
    : _symbols(symbols), result(result) {
}

uint_fast16_t problem_file::truth_table_line::varcount() const {
	return this->_symbols.size();
}

const std::map<ast::symbol, bool> &
problem_file::truth_table_line::symbols() const {
	return this->_symbols;
}
void problem_file::truth_table_line::set_symbol(const ast::symbol &symbol,
                                                bool value) {
	this->_symbols.at(symbol) = value;
}

bool problem_file::operator==(const problem_file::truth_table_line &lhs,
                              const problem_file::truth_table_line &rhs) {
	return lhs.result == rhs.result && lhs._symbols == rhs._symbols;
}
bool problem_file::operator!=(const problem_file::truth_table_line &lhs,
                              const problem_file::truth_table_line &rhs) {
	return lhs.result != rhs.result || lhs._symbols != rhs._symbols;
}
std::partial_ordering
problem_file::operator<=>(const problem_file::truth_table_line &lhs,
                          const problem_file::truth_table_line &rhs) {
	if (lhs._symbols.size() != rhs._symbols.size()) {
		return std::partial_ordering::unordered;
	}
	for (auto left = lhs._symbols.begin(), right = rhs._symbols.begin(),
	          lend = lhs._symbols.end(), rend = rhs._symbols.end();
	     left != lend && right != rend; ++left, ++right) {
		if (left->first != right->first) {
			// Somehow this one has different variables, so these are uncomparable
			return std::partial_ordering::unordered;
		}
		auto result = left->second <=> right->second;
		if (result != 0) {
			return result;
		}
	}
	return std::partial_ordering::equivalent;
}
