#ifndef READ_ERROR__HPP
#define READ_ERROR__HPP

#include <exception>
#include <string>

namespace problem_file {
class read_error : std::exception {
public:
	read_error(std::string &message);
	read_error(std::string &&message);

	std::string message;
	virtual const char *what() const noexcept;
};
} // namespace problem_file

#endif
