#include "ast.hpp"
#include "bit_and.hpp"
#include "bit_not.hpp"
#include "bit_or.hpp"

ast::ast::ast(const enum type type) : type(type) {
}
ast::ast::~ast() {
}

void ast::ast::min() {
	//TODO
}

std::shared_ptr<ast::ast> operator!(std::shared_ptr<ast::ast> &val) {
	if (val->type == ast::type::BIT_NOT) {
		return (std::dynamic_pointer_cast<ast::bit_not>(val)->value);
	}
	else {
		return std::shared_ptr<ast::ast>(new ast::bit_not(val));
	}
}
std::shared_ptr<ast::ast> operator&(std::shared_ptr<ast::ast> &lhs,
                                    std::shared_ptr<ast::ast> &rhs) {
	return std::shared_ptr<ast::ast>(new ast::bit_and(lhs, rhs));
}
std::shared_ptr<ast::ast> operator|(std::shared_ptr<ast::ast> &lhs,
                                    std::shared_ptr<ast::ast> &rhs) {
	return std::shared_ptr<ast::ast>(new ast::bit_or(lhs, rhs));
}
