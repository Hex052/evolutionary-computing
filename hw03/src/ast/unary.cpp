#include "unary.hpp"


ast::unary::unary(const enum type type, std::shared_ptr<ast> &val)
    : ast(type), value(val) {
}
ast::unary::unary(const enum type type, std::shared_ptr<ast> &&val)
    : ast(type), value(val) {
}
ast::unary::~unary() {
}

uint_fast32_t ast::unary::nodes() const {
	return 1 + this->value->nodes();
}
