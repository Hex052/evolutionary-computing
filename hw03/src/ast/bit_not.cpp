#include "bit_not.hpp"
#include "bit_and.hpp"
#include "bit_or.hpp"
#include "symbol.hpp"

// ast::ast *ast::bit_not::copy() const {
// 	return new bit_not(std::shared_ptr<ast>(this->value->copy()));
// }

ast::bit_not::bit_not(std::shared_ptr<ast> &val) : unary(type::BIT_NOT, val) {
}
ast::bit_not::bit_not(std::shared_ptr<ast> &&val)
    : unary(type::BIT_NOT, std::move(val)) {
}
ast::bit_not::bit_not(const bit_not &val)
    : unary(type::BIT_NOT, std::shared_ptr<ast>()) {
	if (std::shared_ptr<bit_and> result =
	      std::dynamic_pointer_cast<bit_and>(val.value)) {
		this->value.reset(new bit_and(*result));
	}
	else if (std::shared_ptr<bit_or> result =
	           std::dynamic_pointer_cast<bit_or>(val.value)) {
		this->value.reset(new bit_or(*result));
	}
	else if (std::shared_ptr<bit_not> result =
	           std::dynamic_pointer_cast<bit_not>(val.value)) {
		this->value.reset(new bit_not(*result));
	}
	else if (std::shared_ptr<symbol> result =
	           std::dynamic_pointer_cast<symbol>(val.value)) {
		this->value.reset(new symbol(*result));
	}
}
ast::bit_not::bit_not(bit_not &&val)
    : unary(type::BIT_NOT, std::move(val.value)) {
}
ast::bit_not::~bit_not() {
}

bool ast::bit_not::eval(const std::map<symbol, bool> &values) const {
	return !this->value->eval(values);
}
