#ifndef AST__SYMBOL__HPP
#define AST__SYMBOL__HPP

namespace ast {
class symbol;
}

#include "ast.hpp"
#include <map>
#include <set>
#include <string>

namespace ast {
/**
 * @brief A variable
 */
class symbol : public ast {
public:
	std::string name;

	symbol(std::string &name);
	symbol(std::string &&name);
	symbol(symbol &val);
	symbol(symbol &&val);
	virtual ~symbol();
	virtual bool eval(const std::map<symbol, bool> &) const;
	virtual uint_fast32_t nodes() const;
};

bool operator==(const symbol &lhs, const symbol &rhs);
bool operator!=(const symbol &lhs, const symbol &rhs);
std::weak_ordering operator<=>(const symbol &lhs, const symbol &rhs);

std::set<symbol> generate_symbols(const uint_fast32_t count);
} // namespace ast

#endif
