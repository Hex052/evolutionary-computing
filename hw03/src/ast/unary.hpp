#ifndef AST__UNARY__HPP
#define AST__UNARY__HPP

#include "ast.hpp"
#include <memory>

namespace ast {
class unary : public ast {
protected:
	unary(const enum type type, std::shared_ptr<ast> &val);
	unary(const enum type type, std::shared_ptr<ast> &&val);
	unary(const unary &);
	unary(unary &&);

public:
	std::shared_ptr<ast> value;
	virtual ~unary();

	virtual bool eval(const std::map<symbol, bool> &) const = 0;
	virtual uint_fast32_t nodes() const;
	virtual std::shared_ptr<ast> &find_node(uint_fast32_t);
};
} // namespace ast

#endif
