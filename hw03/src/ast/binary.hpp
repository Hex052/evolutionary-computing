#ifndef AST__BINARY__HPP
#define AST__BINARY__HPP

#include "ast.hpp"
#include <memory>

namespace ast {
class binary : public ast {
	binary() = delete;

protected:
	binary(const enum type type, std::shared_ptr<ast> &first,
	       std::shared_ptr<ast> &second);
	binary(const enum type type, std::shared_ptr<ast> &&first,
	       std::shared_ptr<ast> &&second);
	binary(const binary &);
	binary(binary &&);

public:
	std::shared_ptr<ast> first;
	std::shared_ptr<ast> second;
	virtual ~binary();

	virtual bool eval(const std::map<symbol, bool> &) const = 0;
	virtual uint_fast32_t nodes() const;
	virtual std::shared_ptr<ast> &find_node(uint_fast32_t);
};
} // namespace ast

#endif
