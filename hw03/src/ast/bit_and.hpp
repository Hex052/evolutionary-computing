#ifndef AST__BIT_AND__HPP
#define AST__BIT_AND__HPP

#include "binary.hpp"

namespace ast {
class bit_and : public binary {
	bit_and() = delete;

public:
	bit_and(std::shared_ptr<ast> &first, std::shared_ptr<ast> &second);
	bit_and(std::shared_ptr<ast> &&first, std::shared_ptr<ast> &&second);
	bit_and(const bit_and &);
	bit_and(bit_and &&);
	~bit_and();
	virtual bool eval(const std::map<symbol, bool> &) const;
};
} // namespace ast

#endif
