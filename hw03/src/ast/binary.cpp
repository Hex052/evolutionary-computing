#include "binary.hpp"

ast::binary::binary(const enum type type, std::shared_ptr<ast> &first,
                    std::shared_ptr<ast> &second)
    : ast(type), first(first), second(second) {
}
