#ifndef AST__BIT_NOT__HPP
#define AST__BIT_NOT__HPP

#include "unary.hpp"

namespace ast {
class bit_not : public unary {
	bit_not() = delete;

protected:
	// virtual ast *copy() const;

public:
	bit_not(std::shared_ptr<ast> &val);
	bit_not(std::shared_ptr<ast> &&val);
	bit_not(const bit_not &);
	bit_not(bit_not &&);
	~bit_not();
	virtual bool eval(const std::map<symbol, bool> &) const;
};
} // namespace ast

#endif
