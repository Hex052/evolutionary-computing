#include "symbol.hpp"
#include "cmath"


ast::symbol::symbol(std::string &name) : ast(type::TERMINAL), name(name) {
}
ast::symbol::symbol(std::string &&name)
    : ast(type::TERMINAL), name(std::move(name)) {
}
ast::symbol::symbol(symbol &val) : ast(type::TERMINAL), name(val.name) {
}
ast::symbol::symbol(symbol &&val)
    : ast(type::TERMINAL), name(std::move(val.name)) {
}
ast::symbol::~symbol() {
}

bool ast::symbol::eval(const std::map<symbol, bool> &values) const {
	return values.at(*this);
}
uint_fast32_t ast::symbol::nodes() const {
	return 0;
}

bool ast::operator==(const symbol &lhs, const symbol &rhs) {
	return lhs.name == rhs.name;
}
bool ast::operator!=(const symbol &lhs, const symbol &rhs) {
	return lhs.name != rhs.name;
}
std::weak_ordering ast::operator<=>(const symbol &lhs, const symbol &rhs) {
	return lhs.name <=> rhs.name;
}

std::set<ast::symbol> ast::generate_symbols(const uint_fast32_t count) {
	uint_fast16_t chars_needed = std::ceil(std::log(count) / std::log(26));
	std::string str(chars_needed, 'a');

	std::set<symbol> result;
	for (uint_fast32_t i = 0; i < count; ++i) {
		result.insert(symbol(str));
		uint_fast16_t j = 0;
		str[0]++;
		while (str[j] == 'z') {
			str[j] = 'a';
			++j;
			str[j] += 1;
		}
	}
	return result;
}
