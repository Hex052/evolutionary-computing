#include "bit_and.hpp"
#include "bit_not.hpp"
#include "bit_or.hpp"
#include "symbol.hpp"

ast::bit_and::bit_and(std::shared_ptr<ast> &first, std::shared_ptr<ast> &second)
    : binary(type::BIT_AND, first, second) {
}
ast::bit_and::bit_and(std::shared_ptr<ast> &&first,
                      std::shared_ptr<ast> &&second)
    : binary(type::BIT_AND, std::move(first), std::move(second)) {
}

ast::bit_and::bit_and(const bit_and &val)
    : binary(type::BIT_AND, std::shared_ptr<ast>(), std::shared_ptr<ast>()) {
	if (std::shared_ptr<bit_and> result =
	      std::dynamic_pointer_cast<bit_and>(val.first)) {
		this->first.reset(new bit_and(*result));
	}
	else if (std::shared_ptr<bit_or> result =
	           std::dynamic_pointer_cast<bit_or>(val.first)) {
		this->first.reset(new bit_or(*result));
	}
	else if (std::shared_ptr<bit_not> result =
	           std::dynamic_pointer_cast<bit_not>(val.first)) {
		this->first.reset(new bit_not(*result));
	}
	else if (std::shared_ptr<symbol> result =
	           std::dynamic_pointer_cast<symbol>(val.first)) {
		this->first.reset(new symbol(*result));
	}

	if (std::shared_ptr<bit_and> result =
	      std::dynamic_pointer_cast<bit_and>(val.second)) {
		this->second.reset(new bit_and(*result));
	}
	else if (std::shared_ptr<bit_or> result =
	           std::dynamic_pointer_cast<bit_or>(val.second)) {
		this->second.reset(new bit_or(*result));
	}
	else if (std::shared_ptr<bit_not> result =
	           std::dynamic_pointer_cast<bit_not>(val.second)) {
		this->second.reset(new bit_not(*result));
	}
	else if (std::shared_ptr<symbol> result =
	           std::dynamic_pointer_cast<symbol>(val.second)) {
		this->second.reset(new symbol(*result));
	}
}
ast::bit_and::bit_and(bit_and &&val)
    : binary(type::BIT_AND, std::move(val.first), std::move(val.second)) {
}

ast::bit_and::~bit_and() {
}
bool ast::bit_and::eval(const std::map<symbol, bool> &values) const {
	return this->first->eval(values) && this->second->eval(values);
}
