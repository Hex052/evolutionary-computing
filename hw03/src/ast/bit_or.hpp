#ifndef AST__BIT_OR__HPP
#define AST__BIT_OR__HPP

#include "binary.hpp"

namespace ast {
class bit_or : public binary {
	bit_or() = delete;

public:
	bit_or(std::shared_ptr<ast> &first, std::shared_ptr<ast> &second);
	// bit_or(std::shared_ptr<ast> &first, std::shared_ptr<ast> &&second);
	// bit_or(std::shared_ptr<ast> &&first, std::shared_ptr<ast> &second);
	bit_or(std::shared_ptr<ast> &&first, std::shared_ptr<ast> &&second);
	bit_or(const bit_or &);
	bit_or(bit_or &&);
	virtual ~bit_or();

	virtual bool eval(const std::map<symbol, bool> &) const;
};
} // namespace ast

#endif
