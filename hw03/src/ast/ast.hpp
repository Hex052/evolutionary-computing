#ifndef AST__AST__HPP
#define AST__AST__HPP

namespace ast {
enum type { BIT_AND, BIT_OR, BIT_NOT, TERMINAL };
class ast;
class symbol; // needs to be declared but can't just include symbol until after
} // namespace ast

#include <cstdint>
#include <map>
#include <memory>

namespace ast {
class ast {
	ast() = delete;

protected:
	ast(const enum type type);

	/**
	 * @brief Copies the AST into a new one.
	 *
	 * This deals with determining what type something is
	 *
	 * @return ast*
	 */
	// virtual ast *copy() const = 0;

public:
	const enum type type;
	virtual ~ast();
	virtual bool eval(const std::map<symbol, bool> &) const = 0;
	/**
	 * @brief Count the number of subnodes. Does not include this node.
	 * @return uint_fast32_t sum of nodes
	 */
	virtual uint_fast32_t nodes() const = 0;

	virtual std::shared_ptr<ast> &find_node(uint_fast32_t) = 0;
	virtual std::shared_ptr<ast> copy(uint_fast32_t) const = 0;


	/**
	 * @brief Minimizes the given AST
	 */
	void min();
};

std::shared_ptr<ast> operator!(std::shared_ptr<ast> &val);
std::shared_ptr<ast> operator&(std::shared_ptr<ast> &lhs,
                               std::shared_ptr<ast> &rhs);
std::shared_ptr<ast> operator|(std::shared_ptr<ast> &lhs,
                               std::shared_ptr<ast> &rhs);

} // namespace ast

#include "symbol.hpp"
#endif
