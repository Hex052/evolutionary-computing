#include "organism.hpp"
#include <cmath>
#include <iomanip>
#include <sstream>

double organism::correct_score = 1.0;
double organism::incorrect_penalty = -0.5;

organism::organism(
  ast::sum_of_prod &expression,
  const problem_file::problem_file::truth_table_type &truth_table)
    : expression(expression), _truth_table(truth_table) {
	this->eval_fitness();
}
organism::~organism() {
}

double organism::fitness() const {
	return this->_fitness;
}
void organism::eval_fitness() {
	this->_fitness = 0;
	for (auto values : this->_truth_table) {
		this->_fitness += (this->expression.eval(values.symbols()) == values.result)
		                    ? organism::correct_score
		                    : organism::incorrect_penalty;
	}
}

std::partial_ordering operator<=>(const organism &lhs, const organism &rhs) {
	std::partial_ordering result = lhs.fitness() <=> rhs.fitness();
	if (result != std::partial_ordering::equivalent) {
		return result;
	}
	// TODO smaller trees win
	return result;
}


organism::operator std::string() const {
	std::stringstream str;
	str << *this;
	return str.str();
}

std::ostream &operator<<(std::ostream &lhs, const organism &rhs) {
	lhs << std::setprecision(4) << std::fixed;
	lhs << "{ expression=" << rhs.expression << ", fitness=" << rhs.fitness()
	    << " }";
	return lhs;
}
