#include "population.hpp"
#include <cmath>
#include <vector>

double population::mutation_chance = .05;
double population::crossover_chance = .85;

// Needed to construct the engine
std::default_random_engine population::random_engine;
std::uniform_real_distribution<double> population::mutation_recombination_dist(
  0, std::nextafter(population::mutation_chance + population::crossover_chance,
                    INFINITY));

population::~population() {
}

void population::seed_generator(uint_fast32_t seed) {
	population::random_engine.seed(seed);
}

population population::breed(uint_fast32_t parent_count) const {
	return this->breed(parent_count, this->size());
}
population population::breed(uint_fast32_t parent_count,
                             uint_fast32_t offspring_count) const {
	if (parent_count > this->size()) {
		throw std::invalid_argument(
		  "cannot have more parents than member of population");
	}
	if (parent_count == 0) {
		throw std::invalid_argument("cannot have zero parents");
	}

	std::vector<const organism &> parents;
	parents.reserve(parent_count);
	{
		auto end = this->rbegin(); // in block to make it fall out of scope
		for (unsigned int i = 0; i < parent_count; ++i) {
			++end;
		}
		parents.insert(parents.begin(), this->rbegin(), end);
	}
	double weights[parent_count];
	for (unsigned int i = 0; i < parent_count; ++i) {
		weights[i] = parents[i].fitness();
	}
	std::discrete_distribution<int> parent_dist(weights, weights + parent_count);
	std::normal_distribution<double> normal_distrib(0.0, 1.0);

	population result(this->_symbols);
	for (unsigned int i = 0; i < offspring_count; ++i) {
		if (mutation_recombination_dist(random_engine) > mutation_chance) {
			// Crossover
			uint_fast32_t rand1, rand2;
			do {
				rand1 = parent_dist(population::random_engine);
				rand2 = parent_dist(population::random_engine);
			} while (rand1 == rand2);

			const organism &parent1 = parents[rand1], &parent2 = parents[rand2];

			std::uniform_int_distribution<uint_fast32_t> node_picker(
			  0U, parent1.expression.nodes());
			auto node1 = parent1.find_node(node_picker(random_engine));
		}
		else {
			// Mutate
			organism parent = parents[parent_dist(population::random_engine)];

			std::shared_ptr<ast::ast> new_ast(parent.expression);
			// Pick a random node
			std::uniform_int_distribution<uint_fast32_t> node_picker(0U,
			                                                         new_ast.nodes());
			uint_fast32_t node = node_picker(random_engine);
			// Find that node
			for (uint_fast32_t i = 0, prev_nodes = 0; i < new_ast.products.size();
			     ++i) {
				if (node < new_ast)
			}

			result.insert(organism(this->rand_sum(), this->_truth_table));
		}
	}
	return result;
}

const organism &population::best() const {
	if (this->size() == 0) {
		throw std::out_of_range("cannot get best from empty population");
	}
	return *(this->rbegin());
}
