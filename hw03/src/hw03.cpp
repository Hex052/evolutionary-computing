#include "population.hpp"
#include "problem_file/problem_file.hpp"
#include <getopt.h>
#include <iostream>
#include <limits.h>

struct args_t {
	/**
	 * @brief Seed for population::seed_generator()
	 */
	population::result_type seed = 1UL;
	/**
	 * @brief Size of the population that the parents are selected from
	 */
	unsigned int pop_size = 27;
	/**
	 * @brief Number of parents to use in each round (top from population)
	 */
	unsigned int parent_count = 3;
	/**
	 * @brief Maximum number of organisms to produce in evolution before stopping
	 */
	unsigned int max_organisms = 10000;
	/**
	 * @brief Maximum number of organisms to produce in evolution before stopping
	 */
	unsigned int max_generations = 10000;
	/**
	 * @brief Path to file that contains the
	 */
	std::string file_path = "problem.txt";
	/**
	 * @brief Maximum number of organisms to produce in evolution before stopping
	 */
	int quiet = 0;
};

enum arg_type {
	SEED = UCHAR_MAX + 2,
	PARENTS,
	POP_SIZE,
	MAX_ORG,
	MAX_GEN,
	PROBLEM_FILE,
	HELP,
	QUIET
};

static void read_args(int argc, char *const *argv, struct args_t *args_out) {
	struct option long_opts[] = {
	  {"seed", required_argument, NULL, arg_type::SEED},
	  {"parents", required_argument, NULL, arg_type::PARENTS},
	  {"pop_size", required_argument, NULL, arg_type::POP_SIZE},
	  {"max_organisms", required_argument, NULL, arg_type::MAX_ORG},
	  {"max_generations", required_argument, NULL, arg_type::MAX_GEN},
	  {"file", required_argument, NULL, arg_type::PROBLEM_FILE},
	  {"help", no_argument, NULL, arg_type::HELP},
	  {"quiet", no_argument, NULL, arg_type::QUIET},
	  {0, 0, 0, 0}};
	int opt;
	while ((opt = getopt_long(argc, argv, "s:r:p:o:g:f:hq", long_opts, NULL))
	       != -1) {
		switch (opt) {
			case 's':
			case arg_type::SEED:
				args_out->seed = atol(optarg);
				break;
			case 'r':
			case arg_type::PARENTS:
				args_out->parent_count = atol(optarg);
				break;
			case 'p':
			case arg_type::POP_SIZE:
				args_out->pop_size = atol(optarg);
				break;
			case 'o':
			case arg_type::MAX_ORG:
				args_out->max_organisms = atol(optarg);
				break;
			case 'g':
			case arg_type::MAX_GEN:
				args_out->max_generations = atol(optarg);
				break;
			case 'f':
			case arg_type::PROBLEM_FILE:
				args_out->file_path = optarg;
				break;
			case 'q':
			case arg_type::QUIET:
				args_out->quiet += 1;
				break;

			case 'h':
			case '?':
			case arg_type::HELP:
				// print help and exit
				/* clang-format off */
				std::cerr <<
				"Usage: " << argv[0] << " [option]\n"
				"  -s\n"
				"  --seed\n"
				"          Seed for the random number generator.\n"
				"          Default 1.\n"
				"  -r\n"
				"  --parents\n"
				"          Number of parents to use in each generation.\n"
				"          Default 3.\n"
				"  -p\n"
				"  --pop_size\n"
				"          Number of organisms to create as the population the\n"
				"          parents are selected from each generation.\n"
				"          Default 27.\n"
				"  -o\n"
				"  --max_organisms\n"
				"          Maximum number of organisms to create across all\n"
				"          generations before assuming we found the best candidate.\n"
				"          Default 10,000.\n"
				"  -g\n"
				"  --max_generations\n"
				"          Maximum number of generations before assuming we found the\n"
				"          best candidate.\n"
				"          Default 10,000.\n"
				"  -f\n"
				"  --file\n"
				"          Location of the file that will configure the program\n"
				"          Default \"problem.txt\".\n"
				"  -h\n"
				"  --help\n"
				"          Display this help and exit.\n"
				"  -q\n"
				"  --quiet\n"
				"          Do not print for each generation.\n"
				<< std::endl;
				/* clang-format on */
				exit(opt == '?' ? 1 : 0);
				break;

			default:
				// Unreachable
				break;
		}
	}
}

int main(int argc, char *const *argv) {
	struct args_t args;
	read_args(argc, argv, &args);

	problem_file::problem_file file(
	  problem_file::problem_file::load_file(args.file_path));
	population::seed_generator(args.seed);

	/// The current population of organisms
	population pop(file.symbols().begin(), file.symbols().end(), args.pop_size,
	               file.truth_table());
	/// The current generation
	unsigned int gen = 0;
	/// The number of organisms that have been generated so far
	unsigned int count = args.pop_size;

	// While the maximum number of generations has not been exceeded
	// and the maximum number of organisms has also not been exceeded
	for (; count < args.max_organisms && gen < args.max_generations;
	     count += args.pop_size, ++gen) {
		if (args.quiet < 1) {
			std::cout << "generation " << gen << ": " << pop.best() << std::endl;
		}
		pop = pop.breed(args.parent_count);
	}
	if (args.quiet < 2) {
		std::cout << "At generation " << gen << " and " << count
		          << " organisms, the best is: ";
	}
	std::cout << pop.best() << std::endl;
	return 0;
}
