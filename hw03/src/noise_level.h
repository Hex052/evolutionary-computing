#ifndef QUIET_LEVEL__HPP
#define QUIET_LEVEL__HPP

enum verbosity { NORMAL, DETAILED, LOUD };

enum verbosity level;

#endif
