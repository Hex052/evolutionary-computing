#ifndef POPULATION_H
#define POPULATION_H

class population;

#include "ast/bit_and.hpp"
#include "ast/bit_or.hpp"
#include "ast/symbol.hpp"
#include "organism.hpp"
#include <memory>
#include <random>
#include <set>
#include <stdexcept>
#include <vector>

class population : public std::set<organism> {
private:
	static double mutation_chance;
	static double crossover_chance;
	static std::default_random_engine random_engine;
	static std::uniform_real_distribution<double> mutation_recombination_dist;

	std::shared_ptr<std::vector<ast::symbol>> _symbols;
	const problem_file::problem_file::truth_table_type &_truth_table;
	const std::uniform_int_distribution<int> parameter_dist;

	population() = delete;
	population(population &);
	population(const std::shared_ptr<std::vector<ast::symbol>> &symbols);
	population(std::shared_ptr<std::vector<ast::symbol>> &&symbols);

	ast::symbol &rand_symbol() const;
	ast::bit_and rand_product() const;
	ast::bit_or rand_sum() const;

public:
	typedef std::default_random_engine::result_type result_type;
	static constexpr result_type default_seed =
	  std::default_random_engine::default_seed;

	/**
	 * @brief Construct a new population object full of randomly created
	 * organisms using some list of symbols
	 *
	 * @tparam itertype Type of @p begin and @p end
	 * @param begin Beginning of the iterable over symbols
	 * @param end End of the iterable over symbols
	 * @param initial_size Initial population size
	 */
	template <typename itertype>
	population(itertype begin, itertype end, uint_fast32_t initial_size,
	           const problem_file::problem_file::truth_table_type &truth_table)
	    : set(), _symbols(new std::vector<ast::symbol>(begin, end)),
	      _truth_table(truth_table), parameter_dist(0, this->_symbols->size()) {
		for (unsigned int i = 0; i < initial_size; ++i) {
			this->insert(organism(this->rand_sum(), truth_table));
		}
	}

	population &operator=(const population &rhs);
	population &operator=(population &&rhs);

	// using fitness_iterator = fitness_iterator;
	static void seed_generator(uint_fast32_t seed);

	virtual ~population();

	/**
	 * @brief Breed a new population from the top @p parent_count
	 *
	 * @param parent_count mu number of parents
	 * @return population new population of the same size as the current one
	 */
	population breed(uint_fast32_t parent_count) const;
	/**
	 * @brief Breed a new population from the top @p parent_count
	 *
	 * @param parent_count mu number of parents
	 * @param offspring_count lambda number of offspring
	 * @return population new population of size @p offspring_count
	 */
	population breed(uint_fast32_t parent_count,
	                 uint_fast32_t offspring_count) const;

	/**
	 * @brief Gets the best member or throws std::out_of_range if empty
	 *
	 * @return const organism& The organism with the highest fitness value
	 * @throw std::out_of_range No organisms in population
	 */
	const organism &best() const;
};

#endif
