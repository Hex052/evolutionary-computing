#ifndef ORGANISM_H
#define ORGANISM_H

class organism;

#include "ast/ast.hpp"
#include "population.hpp"
#include "problem_file/problem_file.hpp"
#include <compare>
#include <ostream>
#include <string>

class organism {
	friend class population;
	friend std::ostream &operator<<(std::ostream &lhs, const organism &rhs);

private:
	/// Beneft for a correct fitness evaluation
	static double correct_score;
	/// Penalty for an incorrect fitness evaluation
	static double incorrect_penalty;

	double _fitness;
	/// Uneditable truth table
	const problem_file::problem_file::truth_table_type &_truth_table;
	/// Expression
	std::shared_ptr<ast::ast> expression;

	/**
	 * @brief Reevaluate the value of @p _fitness
	 */
	void eval_fitness();

	organism() = delete;
	organism(std::shared_ptr<ast::ast> &,
	         const problem_file::problem_file::truth_table_type &);
	organism(std::shared_ptr<ast::ast> &&,
	         const problem_file::problem_file::truth_table_type &);

public:
	virtual ~organism();

	constexpr double fitness() const;

	operator std::string() const;
};
bool operator==(const organism &lhs, const organism &rhs);
bool operator!=(const organism &lhs, const organism &rhs);
std::partial_ordering operator<=>(const organism &lhs, const organism &rhs);

std::ostream &operator<<(std::ostream &lhs, const organism &rhs);

#endif
