# CSCE A412 Evolutionary Computing HW 3

Elijah Faso-Formoso  
2021-04-07

- `minrandom` and `maxrandom` have been removed as they were no longer necesary
- `randomnumber` (NRAND) was removed because constants in a boolean system
don't make a ton of sense
- The fitness function was altered to penalize each one by -0.5 when an answer
was wrong.
This turned out to be completely useless because its either wrong or right,
there's no middle ground.
But I left it in.

## Two problems that I got hung up on

- Spent a long time before I figured out `System.arraycopy` was source then
destination when I was assuming it was the other way around.
I'm probably just more used to other languages which work hard to be
consistently destination then source, like how the assignment operator works.
- The negative signs in the fitness I left in thinking the implementation
needed it that way, but that wasn't the case. Left me stuck at the best
fitness value being -8 for the longest time.

## Potential improvements

- Once 16 is reached, score the most fit as being the shortest one.
This would allow the sum of products or product of sums to be reached,
rather than the huge mess of an equation that is currently produced.
- Create a function to automatically remove double NOT as it 
doesn't do anything
- Trying to recreate new trees for mutation may have been a mistake as it
doesn't really converge

## Results table

This was run with seed 358514

| Min SOP F(A, B, C, D) =     | Best of Run Expression F(A, B, C, D) =                                                                                                                                          | Best Fitness | Generation | Example File used |
|-----------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|------------|-------------------|
| A’C’ + A’B + ACD            | ((X1  & ( !  ! (X3  \| (((( ! X2  & X1 ) & X1 ) \| X2 ) &  ! (X1  & X4 ))) & X4 )) \| (((( ! X3  &  ! X1 ) \| X2 ) &  ! X1 ) &  ! X1 ))                                         | 16           | 8          | `problem.txt`       |
| A’B’C + AB’C’ + ABC’ + ACD’ | (X3  & (((X3  \|  ! X3 ) &  ! X2 ) \| ((X3  \| ((( ! (X2  & ((X4  \| ((X2  \| X3 ) & X3 )) \| ((X3  \| X2 ) & X3 ))) & X4 ) \| ( !  ! X4  & X1 )) \| X3 )) & (X3  & X1 ))))     | 2            | 16         | `problem_3.txt`     |
| A’B’ + A’BC + AB’C          | ( ! (X2  & ((X4  \| X2 ) \| ((( !  ! X1  \|  !  ! X3 ) \| ( ! (X2  & X3 ) \|  ! (X2  & X1 ))) \| X2 ))) \| ((X1  \|  ! X1 ) \|  ! ( !  ! X1  & (X2  \| ((X1  \| X2 ) & X4 ))))) | 2            | 99         | `problem_4.txt`     |
| A’B’ + B’(C’D’+CD) + ABC    | ( ! (X2  & ((X4  \| X2 ) \| ((( !  ! X1  \|  !  ! X3 ) \| ( ! (X2  & X3 ) \|  ! (X2  & X1 ))) \| X2 ))) \| ((X1  \|  ! X1 ) \|  ! ( !  ! X1  & (X2  \| ((X1  \| X2 ) & X4 ))))) | 2            | 99         | `problem_5.txt`     |

They somehow produced identical output for `problem_4.txt` and `problem_5.txt`
