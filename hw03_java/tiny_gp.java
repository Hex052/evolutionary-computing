/*
 * Program:   tiny_gp.java
 *
 * Author:    Riccardo Poli (email: rpoli@essex.ac.uk)
 *
 */

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashSet;
import java.util.Random;
import java.util.StringTokenizer;

public class tiny_gp {
  /**
   * Fitness scores of the organisms in the population
   */
  double[] fitness;
  /**
   * Number of organisms in the population
   */
  char[][] pop;
  /**
   * Random number generator
   */
  static Random rd = new Random();
  static final char AND = 120;
  static final char OR = AND + 1;
  static final char NOT = OR + 1;
  static final char FSET_START = AND;
  static final char FSET_END = NOT;
  /**
   * Variable values for this particular row in the fitness table
   */
  static boolean[] values = new boolean[FSET_START];
  /**
   * The values from the truth table. Second dimension is length varnumber + 1, to
   * account for extra result.
   */
  static boolean[][] targets;
  /**
   * The program that is being evolved
   */
  static char[] program;
  /**
   * Position in the program of the current evaluation
   */
  static int PC;
  /**
   * Number of variables
   */
  static int varnumber;
  /**
   * Number of fitness cases
   */
  static int fitnesscases;
  /**
   * integer representing the number of random constants to be provided in the
   * primitive set
   */
  static int randomnumber;
  /**
   * The best fitness value of the population
   */
  static double fbestpop = 0.0;
  /**
   * The best fitness value of the population
   */
  static double fperfect;
  /**
   * The average fitness value from the population
   */
  static double favgpop = 0.0;
  /**
   * Seed for the random number generator
   */
  static long seed;
  static double avg_len;
  static final int MAX_LEN = 10000;
  // static final int POPSIZE = 100000;
  static final int POPSIZE = 1000;
  static final int DEPTH = 5;
  static final int GENERATIONS = 100;
  static final int TSIZE = 2;
  public static final double PMUT_PER_NODE = 0.05;
  static final double CROSSOVER_PROB = 0.9;

  /**
   * Interpreter for the machine
   *
   * @return Result of evaluation
   */
  boolean run() { /* Interpreter */
    char primitive = program[PC++];
    if (primitive < FSET_START)
      return (values[primitive]);
    switch (primitive) {
    case AND:
      // Need to run with non-short-circuit operators so the PC gets incremented
      return (run() & run());
    case OR:
      return (run() | run());
    case NOT:
      // Skip the first return value
      // run();
      return (!run());
    }
    return (false); // should never get here
  }

  /**
   *
   * @param buffer      Program being analyzed
   * @param buffercount Current position in the program
   * @return The number of nodes in the program
   */
  int traverse(char[] buffer, int buffercount) {
    if (buffer[buffercount] < FSET_START)
      return (++buffercount);

    switch (buffer[buffercount]) {
    case AND:
    case OR:
      // case NOT:
      return traverse(buffer, traverse(buffer, ++buffercount));
    case NOT:
      return traverse(buffer, ++buffercount);
    }
    return (0); // should never get here
  }

  /**
   * Parses the setup file
   *
   * @param fname path to file to parse
   */
  void load_file(String fname) {
    BufferedReader in = null;
    try {
      String line;

      in = new BufferedReader(new FileReader(fname));
      line = in.readLine();
      StringTokenizer tokens = new StringTokenizer(line);
      varnumber = Integer.parseInt(tokens.nextToken().trim());
      fperfect = 1 << varnumber;
      fitnesscases = Integer.parseInt(tokens.nextToken().trim());
      targets = new boolean[fitnesscases][varnumber + 1];
      if (varnumber >= FSET_START) {
        System.out.println("too many variables");
      }

      HashSet<boolean[]> previous = new HashSet<>();
      for (int line_num = 0; line_num < fitnesscases; line_num++) {
        line = in.readLine();
        tokens = new StringTokenizer(line);
        for (int j = 0; j <= varnumber; j++) {
          targets[line_num][j] = parseBoolean(tokens.nextToken().trim());
        }
        boolean[] values = new boolean[varnumber];
        System.arraycopy(targets[line_num], 0, values, 0, varnumber);
        if (!previous.add(values)) {
          System.out.println("ERROR: Duplicate line at " + line_num + 2);
          System.exit(0);
        }
      }
      in.close();
    } catch (FileNotFoundException e) {
      System.out.println("ERROR: Please provide a data file");
      System.exit(0);
    } catch (IllegalArgumentException e) {
      System.out.println("ERROR: " + e.getMessage());
      System.exit(0);
    } catch (Exception e) {
      System.out.println("ERROR: Incorrect data format. " + e.getMessage());
      System.exit(0);
    } finally {
      if (in != null) {
        try {
          in.close();
        } catch (IOException exc) {
        }
      }
    }
  }

  static boolean parseBoolean(String str) throws IllegalArgumentException {
    if (str.length() == 1 && ("t".equalsIgnoreCase(str) || "y".equalsIgnoreCase(str) || "1".equals(str))
        || "true".equalsIgnoreCase(str) || "on".equalsIgnoreCase(str) || "yes".equalsIgnoreCase(str)) {
      // true, t, on, yes, y, 1
      return true;
    } else if (str.length() == 1 && ("f".equalsIgnoreCase(str) || "n".equalsIgnoreCase(str) || "0".equals(str))
        || "false".equalsIgnoreCase(str) || "off".equalsIgnoreCase(str) || "no".equalsIgnoreCase(str)) {
      // false, f, off, no, n, 0
      return false;
    } else {
      throw new IllegalArgumentException("cannot convert string \"" + str + "\" to bool");
    }
  }

  /**
   * Computes the fitness of a program
   *
   * @param Prog Program to evaluate
   * @return Fitness value
   */
  double fitness_function(char[] Prog) {
    double fit = 0.0;

    for (int i = 0; i < fitnesscases; i++) {
      System.arraycopy(targets[i], 0, values, 0, varnumber);
      program = Prog;
      PC = 0;
      boolean result = run();
      // if (result == targets[i][varnumber])
      // fit += 1;
      fit += (result == targets[i][varnumber]) ? 1 : -.5;
    }
    return fit;
  }

  /**
   * Generate a program or a tree in part of a program
   *
   * @param buffer Program being generated
   * @param pos    Next blank position
   * @param depth  Depth remaining that can be used
   * @return the length of the new child or a value less than 0 if space was
   *         exhausted
   */
  int grow(char[] buffer, int pos, int depth) {
    char prim;

    if (pos >= buffer.length)
      return (-1);

    if (pos == 0)
      prim = 1;
    else
      prim = (char) rd.nextInt(2);

    if (prim == 0 || depth == 0) {
      prim = (char) rd.nextInt(varnumber);
      buffer[pos] = prim;
      return (pos + 1);
    } else {
      prim = (char) (rd.nextInt(FSET_END - FSET_START + 1) + FSET_START);
      int one_child;
      buffer[pos] = prim;
      switch (prim) {
      case AND:
      case OR:
        // case NOT:
        one_child = grow(buffer, pos + 1, depth - 1);
        if (one_child < 0) {
          return -1;
        } else {
          return grow(buffer, one_child, depth - 1);
        }
      case NOT:
        return grow(buffer, pos + 1, depth - 1);
      }
    }
    return 0; // should never get here
  }

  static char[] buffer = new char[MAX_LEN];

  /**
   * Create a new individual
   *
   * @param depth Depth to create
   * @return A randomly created individual with the specified depth
   */
  char[] create_random_indiv(int depth) {
    char[] ind;
    int len;
    do {
      len = grow(buffer, 0, depth);
    } while (len < 0);

    ind = new char[len];

    System.arraycopy(buffer, 0, ind, 0, len);
    return (ind);
  }

  /**
   * Prints the individual to the output
   *
   * @param buffer
   * @param buffercounter
   * @return Next position in buffer
   */
  int print_indiv(char[] buffer, int buffercounter) {
    int a1 = 0, a2;
    // for (char c : buffer) {
    // System.out.print("" + (int) c + " ");
    // }
    if (buffer[buffercounter] < FSET_START) {
      if (buffer[buffercounter] < varnumber)
        System.out.print("X" + (buffer[buffercounter] + 1) + " ");
      else
        System.out.print(values[buffer[buffercounter]]);
      return (++buffercounter);
    }
    switch (buffer[buffercounter]) {
    case AND:
      System.out.print("(");
      a1 = print_indiv(buffer, ++buffercounter);
      System.out.print(" & ");
      a2 = print_indiv(buffer, a1);
      System.out.print(")");
      return (a2);
    case OR:
      System.out.print("(");
      a1 = print_indiv(buffer, ++buffercounter);
      System.out.print(" | ");
      a2 = print_indiv(buffer, a1);
      System.out.print(")");
      return (a2);
    case NOT:
      System.out.print(" ! ");
      // buffercounter += 2; // The first gets skipped
      a1 = print_indiv(buffer, ++buffercounter);
      return (a1);
    }
    return 0; // Unreachable
  }

  /**
   * Create n random population members
   *
   * @param n       Number to creat
   * @param depth   Depth of the members to be created
   * @param fitness Fitness scores for those indidivuals
   * @return
   */
  char[][] create_random_pop(int n, int depth, double[] fitness) {
    char[][] pop = new char[n][];
    for (int i = 0; i < n; i++) {
      pop[i] = create_random_indiv(depth);
      fitness[i] = fitness_function(pop[i]);
    }
    return (pop);
  }

  /**
   * Sets global variables fbestpop and favgpop and prints the best one
   *
   * @param fitness
   * @param pop
   * @param gen
   */
  void stats(double[] fitness, char[][] pop, int gen) {
    int i, best = rd.nextInt(POPSIZE);
    int node_count = 0;
    fbestpop = fitness[best];
    favgpop = 0.0;

    for (i = 0; i < POPSIZE; i++) {
      node_count += traverse(pop[i], 0);
      favgpop += fitness[i];
      if (fitness[i] > fbestpop) {
        best = i;
        fbestpop = fitness[i];
      }
    }
    avg_len = (double) node_count / POPSIZE;
    favgpop /= POPSIZE;
    System.out.println(
        "Generation=" + gen + " Avg Fitness=" + favgpop + " Best Fitness=" + fbestpop + " Avg Size=" + avg_len);
    System.out.print("Best Individual: ");
    print_indiv(pop[best], 0);
    System.out.print("\n");
    System.out.flush();
  }

  int tournament(double[] fitness, int tsize) {
    int best = rd.nextInt(POPSIZE), i, competitor;
    double fbest = -1.0e34;

    for (i = 0; i < tsize; i++) {
      competitor = rd.nextInt(POPSIZE);
      if (fitness[competitor] > fbest) {
        fbest = fitness[competitor];
        best = competitor;
      }
    }
    return (best);
  }

  int negative_tournament(double[] fitness, int tsize) {
    int worst = rd.nextInt(POPSIZE), i, competitor;
    double fworst = 1e34;

    for (i = 0; i < tsize; i++) {
      competitor = rd.nextInt(POPSIZE);
      if (fitness[competitor] < fworst) {
        fworst = fitness[competitor];
        worst = competitor;
      }
    }
    return (worst);
  }

  char[] crossover(char[] parent1, char[] parent2) {
    int xo1start, xo1end, xo2start, xo2end;
    char[] offspring;
    int len1 = traverse(parent1, 0);
    int len2 = traverse(parent2, 0);
    int lenoff;

    xo1start = rd.nextInt(len1);
    xo1end = traverse(parent1, xo1start);

    xo2start = rd.nextInt(len2);
    xo2end = traverse(parent2, xo2start);

    lenoff = xo1start + (xo2end - xo2start) + (len1 - xo1end);

    offspring = new char[lenoff];

    System.arraycopy(parent1, 0, offspring, 0, xo1start);
    System.arraycopy(parent2, xo2start, offspring, xo1start, (xo2end - xo2start));
    System.arraycopy(parent1, xo1end, offspring, xo1start + (xo2end - xo2start), (len1 - xo1end));

    return (offspring);
  }

  /**
   * Mutates a program by picking a node and regrowing a tree
   *
   * @param parent Parent that will be mutated
   * @param pmut   Probability of mutation
   * @return New child
   */
  char[] mutation(char[] parent, double pmut) {
    int len = traverse(parent, 0);

    int trim_pos = rd.nextInt(len);
    char[] new_limb = new char[MAX_LEN];
    int removed_end = traverse(parent, trim_pos);
    int limb_len;
    do {
      limb_len = grow(new_limb, 0, DEPTH);
    } while (limb_len < 0);

    char[] child = new char[len - (removed_end - trim_pos) + limb_len];

    System.arraycopy(parent, 0, child, 0, trim_pos);
    System.arraycopy(new_limb, 0, child, trim_pos, limb_len);
    System.arraycopy(parent, removed_end, child, trim_pos + limb_len, len - (removed_end - trim_pos) - trim_pos);

    // char[] child = new char[len];
    // for (int i = 0; i < len; i++) {
    // if (rd.nextDouble() < pmut) {
    // int mutsite = i;
    // if (child[mutsite] < FSET_START)
    // child[mutsite] = (char) rd.nextInt(varnumber);
    // else {
    // char new_operator = (char) (rd.nextInt(FSET_END - FSET_START + 1) +
    // FSET_START);
    // child[mutsite] = new_operator;
    // }
    // }
    // }
    return child;
  }

  /**
   * Print the parameters of the file
   */
  void print_parms() {
    System.out.print("-- TINY GP (Java version) --\n");
    System.out.print("SEED=" + seed + "\n");
    System.out.print("MAX_LEN=" + MAX_LEN + "\n");
    System.out.print("POPSIZE=" + POPSIZE + "\n");
    System.out.print("DEPTH=" + DEPTH + "\n");
    System.out.print("CROSSOVER_PROB=" + CROSSOVER_PROB + "\n");
    System.out.print("PMUT_PER_NODE=" + PMUT_PER_NODE + "\n");
    System.out.print("GENERATIONS=" + GENERATIONS + "\n");
    System.out.print("TSIZE=" + TSIZE + "\n");
    System.out.print("----------------------------------\n");
  }

  public tiny_gp(String fname, long s) {
    fitness = new double[POPSIZE];
    seed = s;
    if (seed >= 0)
      rd.setSeed(seed);
    load_file(fname);
    pop = create_random_pop(POPSIZE, DEPTH, fitness);
  }

  // char[] reduce(char[] prog) {
  //
  // }

  /**
   * Main method of the algorithm
   */
  void evolve() {
    int gen = 0, offspring, parent1, parent2, parent;
    double newfit;
    char[] newind;
    print_parms();
    stats(fitness, pop, 0);
    for (gen = 1; gen < GENERATIONS; gen++) {
      if (fbestpop >= fperfect) {
        System.out.print("PROBLEM SOLVED\n");
        System.exit(0);
      }
      for (int indivs = 0; indivs < POPSIZE; indivs++) {
        if (rd.nextDouble() < CROSSOVER_PROB) {
          parent1 = tournament(fitness, TSIZE);
          parent2 = tournament(fitness, TSIZE);
          newind = crossover(pop[parent1], pop[parent2]);
        } else {
          parent = tournament(fitness, TSIZE);
          newind = mutation(pop[parent], PMUT_PER_NODE);
        }
        newfit = fitness_function(newind);
        offspring = negative_tournament(fitness, TSIZE);
        pop[offspring] = newind;
        fitness[offspring] = newfit;
      }
      stats(fitness, pop, gen);
    }
    System.out.print("PROBLEM *NOT* SOLVED\n");
    System.exit(1);
  }

  public static void main(String[] args) {
    String fname = "problem.txt";
    long s = -1;

    if (args.length == 2) {
      s = Integer.valueOf(args[0]).intValue();
      fname = args[1];
    } else if (args.length == 1) {
      fname = args[0];
    }

    tiny_gp gp = new tiny_gp(fname, s);
    gp.evolve();
  }
};
