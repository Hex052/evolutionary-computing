"""
Classes for organism and population
"""

from .objects import Knapsack, KnapsackObject
from typing import Any, Iterable, List, Optional, Sequence, Union
import itertools
import operator
import random


class Organism(tuple):
    """
    Represents organisms int the population
    """

    # Creates new organizim inherited from tuple
    def __new__(cls, knapsack: Knapsack, values: Iterable[bool]):
        obj = super().__new__(cls, values)
        # obj._fitness: Union[float, None] = None
        return obj

    # Same thing as above but for organizim
    def __init__(self, knapsack: Knapsack, values: Iterable[bool]) -> None:
        super().__init__()
        # Either fitness wil be null or have a float
        self._fitness: Union[float, None] = None
        self._knapsack: Knapsack = knapsack

    def selected(self) -> Iterable[KnapsackObject]:
        return map(
            lambda x: x[1],
            filter(
                lambda x: x[0],
                zip(self, self._knapsack.objects),
            ),
        )

    @property
    def fitness(self) -> float:
        if self._fitness is None:
            self._fitness = 0
            weight = 0
            for knapsackobj in self.selected():
                self._fitness += knapsackobj.value
                weight += knapsackobj.weight
            if weight > self._knapsack.capacity:
                self._fitness = -self._fitness
        return self._fitness

    @property
    def knapsack(self) -> Knapsack:
        return self._knapsack

    @knapsack.setter
    def knapsack(self, value: Knapsack) -> None:
        self._fitness = None
        self._knapsack = value

    def mutate(self) -> "Organism":
        if random.random() < self.knapsack.chance.mutation:
            place = random.randint(0, len(self) - 1)
            return Organism(
                self._knapsack,
                itertools.chain(self[:place], (not self[place],), self[place + 1 :]),
            )
        else:
            return self

    def crossover(self, other: "Organism") -> "Organism":
        if self is other:
            return self
        if len(self) != len(other):
            raise ValueError("len(self) != len(other)")
        if self._knapsack.chance.crossover != other._knapsack.chance.crossover:
            # Raise an exception because we don't know which is right
            raise ValueError(
                "Configured crossover chance is different between"
                f" self ({self._knapsack.chance.crossover})"
                f" and other ({other._knapsack.chance.crossover})"
            )
        if random.random() < self._knapsack.chance.crossover:
            switch_point = random.randint(1, len(self) - 2)
            return Organism(
                self._knapsack,
                itertools.chain(self[:switch_point], other[switch_point:]),
            )
        return self

    def __or__(self, other: Any) -> "Organism":
        if not isinstance(other, Organism):
            return NotImplemented
        try:
            return self.crossover(other)
        except ValueError:
            return NotImplemented

    def __inv__(self) -> "Organism":
        return Organism(self._knapsack, *map(operator.not_, self))

    @classmethod
    def random(
        cls,
        knapsack: Knapsack,
        chance_included: Optional[float] = 0.5,
    ) -> "Organism":
        return Organism(
            knapsack, ((random.random() < chance_included) for _ in knapsack.objects)
        )

    def __eq__(self, other) -> bool:
        if not isinstance(other, Organism):
            return NotImplemented
        return super().__eq__(other)

    def __ne__(self, other) -> bool:
        if not isinstance(other, Organism):
            return NotImplemented
        return super().__ne__(other)

    def __gt__(self, other) -> bool:
        if not isinstance(other, Organism):
            return NotImplemented
        return self.fitness > other.fitness

    def __ge__(self, other) -> bool:
        if not isinstance(other, Organism):
            return NotImplemented
        return self.fitness >= other.fitness

    def __lt__(self, other) -> bool:
        if not isinstance(other, Organism):
            return NotImplemented
        return self.fitness < other.fitness

    def __le__(self, other) -> bool:
        if not isinstance(other, Organism):
            return NotImplemented
        return self.fitness <= other.fitness

    def __str__(self) -> str:
        objects = ", ".join(map(lambda x: x.name, self.selected()))
        return f"<[{objects}], fitness {self.fitness:.4f}>"


class Population(object):
    def __init__(self, organisms: Sequence[Organism], config: Knapsack) -> None:
        self.organisms = organisms
        self.config = config

    def __len__(self) -> int:
        return len(self._organisms)

    @classmethod
    def random_pop(cls, config: Knapsack) -> "Population":
        """
        Produces a random population with at least one member with a positive
        fitness. None will be returned if there is no solution.

        :return: [description]
        :rtype: [type]
        """
        heaviest = max(map(operator.attrgetter("weight"), config.objects))
        if heaviest > config.capacity:
            # Unsolvable. Return the most boring population.
            return Population(
                tuple(
                    itertools.repeat(
                        Organism(config, (False for _ in config.objects)),
                        config.per_generation,
                    )
                ),
                config,
            )
        for chance in (0.5, 0.45, 0.4, 0.35, 0.3, 0.25, 0.2, 0.15, 0.1, 0.05):
            organisms = [
                Organism.random(config, chance) for _ in range(config.per_generation)
            ]
            organisms.sort()
            if organisms[-1].fitness > 0:
                return Population(tuple(organisms), config)
        # Include everything, I guess
        return Population(
            tuple(
                itertools.repeat(
                    Organism(config, (True for _ in config.objects)),
                    config.per_generation,
                )
            ),
            config,
        )

    def breed(self) -> "Population":
        if self.config.parents_per_generation <= 0:
            raise ValueError("Cannot have zero or negative parent count")
        if self.config.parents_per_generation > self.config.per_generation:
            raise ValueError("Cannot have more parents than organisms in population")
        parents = self.organisms[-self.config.parents_per_generation :]
        next_gen: List[Organism] = [
            (random.choice(parents) | random.choice(parents)).mutate()
            for _ in range(self.config.per_generation)
        ]
        next_gen.sort()
        return Population(tuple(next_gen), self.config)

    def best(self) -> Organism:
        return self.organisms[-1]
