"""
Objects that might be imported from
"""
from typing import List, Optional, overload
import yaml

safe_load = False


class KnapsackObject(yaml.YAMLObject):
    yaml_tag = "!KnapsackObject"
    yaml_loader = yaml.SafeLoader if safe_load else yaml.Loader

    def __init__(self, name: str, weight: float, value: float) -> None:
        self.name = name
        self.weight = weight
        self.value = value

    @classmethod
    def from_yaml(cls, loader, node):
        if not isinstance(node, yaml.MappingNode) or node.tag not in (
            cls.yaml_tag,
            loader.DEFAULT_MAPPING_TAG,
        ):
            raise ValueError(
                "expected yaml map or KnapsackObject"
                f" (tags {loader.DEFAULT_MAPPING_TAG} or {cls.yaml_tag})"
            )
        return super().from_yaml(loader, node)


class MutationChance(yaml.YAMLObject):
    yaml_tag = "!MutationChance"
    yaml_loader = yaml.SafeLoader if safe_load else yaml.Loader

    @overload
    def __new__(cls, mutation: float, crossover: float) -> "MutationChance":
        ...

    @overload
    def __new__(cls) -> "MutationChance":
        ...

    def __new__(cls, *args, **kwargs) -> "MutationChance":
        obj = super().__new__(cls)
        obj.__init__(*args, **kwargs)
        return obj

    def __init__(
        self,
        mutation: Optional[float] = None,
        crossover: Optional[float] = None,
    ):
        self.mutation = mutation if mutation is not None else 0.05
        self.crossover = crossover if crossover is not None else 0.9

    @classmethod
    def from_yaml(cls, loader, node):
        if not isinstance(node, yaml.MappingNode) or node.tag not in (
            cls.yaml_tag,
            loader.DEFAULT_MAPPING_TAG,
        ):
            raise ValueError(
                "expected yaml map or MutationChance"
                f" (tags {loader.DEFAULT_MAPPING_TAG} or {cls.yaml_tag})"
            )
        return super().from_yaml(loader, node)


class Knapsack(yaml.YAMLObject):
    yaml_tag = "!Knapsack"
    yaml_loader = yaml.SafeLoader if safe_load else yaml.Loader

    @overload
    def __new__(
        cls,
        generations: int,
        per_generation: int,
        capacity: float,
        chance: MutationChance,
        objects: List[KnapsackObject],
    ) -> "Knapsack":
        ...

    @overload
    def __new__(cls) -> "Knapsack":
        ...

    def __new__(cls, *args, **kwargs) -> "Knapsack":
        self = super().__new__(cls)
        self.__init__(*args, **kwargs)
        return self

    def __init__(
        self,
        seed: Optional[int] = None,
        generations: Optional[int] = None,
        per_generation: Optional[int] = None,
        parents_per_generation: Optional[int] = None,
        capacity: Optional[float] = None,
        chance: Optional[MutationChance] = None,
        objects: Optional[List[KnapsackObject]] = None,
    ):
        self.seed = seed if seed is not None else 1
        self.generations = generations if generations is not None else 1000
        self.per_generation = per_generation if per_generation is not None else 150
        self.parents_per_generation = (
            parents_per_generation if parents_per_generation is not None else 50
        )
        self.capacity = capacity if capacity is not None else 41.0
        self.chance = chance if chance is not None else MutationChance()
        self.objects = objects if objects is not None else list()

    @classmethod
    def from_yaml(cls, loader: yaml.BaseLoader, node) -> "Knapsack":
        if not isinstance(node, yaml.MappingNode) or node.tag not in (
            cls.yaml_tag,
            loader.DEFAULT_MAPPING_TAG,
        ):
            raise ValueError(
                "expected yaml map or Knapsack"
                f" (tags {loader.DEFAULT_MAPPING_TAG} or {cls.yaml_tag})"
            )
        return super().from_yaml(loader, node)


def load_config(path: str) -> Knapsack:
    with open(path, mode="r", encoding="utf-8-sig") as stream:
        loader = yaml.SafeLoader(stream) if safe_load else yaml.Loader(stream)
    try:
        loader.add_path_resolver(Knapsack.yaml_tag, [])
        loader.add_path_resolver(
            KnapsackObject.yaml_tag,
            [(yaml.MappingNode, "objects"), (yaml.SequenceNode, None)],
        )
        loader.add_path_resolver(
            MutationChance.yaml_tag, [(yaml.MappingNode, "chance")]
        )
        return loader.get_single_data()
    finally:
        loader.dispose()
