from typing import Optional, Sequence, Tuple
from . import main
from .objects import Knapsack, load_config
import argparse
import os


def parse_args(argv: Optional[Sequence[str]] = None) -> Tuple[Knapsack]:
    example_config = os.path.join(os.path.dirname(__file__), "config.yaml")
    argparser = argparse.ArgumentParser("knapsack")
    argparser.add_argument(
        "config_file",
        type=load_config,
        help="Path to the configuration file, in YAML format.{example_config}"
        " An example file is provided in {example_config}".format(
            example_config=example_config
        ),
    )
    args = argparser.parse_args(argv)
    return (args.config_file,)


main(*parse_args())
