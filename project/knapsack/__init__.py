from .objects import Knapsack
from .population import Organism, Population
import random


def display_best(generation: int, organisms: int, best_organism: Organism):
    selected_names = {i.name for i in best_organism.selected()}
    print(
        f"At generation {generation} and {organisms} organisms, best has"
        f" {selected_names} and a fitness of {best_organism.fitness:.4f}"
    )


def main(config: Knapsack):
    random.seed(config.seed)
    population = Population.random_pop(config)
    generation = 0
    organisms = config.per_generation
    display_best(generation, organisms, population.best())
    for generation in range(1, config.generations + 1):
        population = population.breed()
        organisms += config.per_generation
        display_best(generation, organisms, population.best())
