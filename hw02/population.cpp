#include "population.hpp"
#include <math.h>
#include <vector>

std::default_random_engine population::random_engine;

population::population() : set() {
}
population::population(unsigned int initial_size) : set() {
	std::uniform_real_distribution uniform_distrib_x1(
	  organism::x1_min, std::nextafter(organism::x1_max, INFINITY));
	std::uniform_real_distribution uniform_distrib_x2(
	  organism::x2_min, std::nextafter(organism::x2_max, INFINITY));
	for (unsigned int i = 0; i < initial_size; ++i) {
		this->insert(organism(uniform_distrib_x1(population::random_engine),
		                      uniform_distrib_x2(population::random_engine), 1, 1));
	}
}

population::~population() {
}

void population::seed_generator(uint_fast32_t seed) {
	population::random_engine.seed(seed);
}

population population::breed(unsigned int parent_count) const {
	return this->breed(parent_count, this->size());
}
population population::breed(unsigned int parent_count,
                             unsigned int offspring_count) const {
	if (parent_count > this->size()) {
		parent_count = this->size();
	}
	if (parent_count == 0) {
		throw std::invalid_argument("cannot have zero parents");
	}

	std::vector<population::value_type> parents;
	parents.reserve(parent_count);
	{
		auto end = this->rbegin(); // in block to make it fall out of scope
		for (unsigned int i = 0; i < parent_count; ++i) {
			++end;
		}
		parents.insert(parents.begin(), this->rbegin(), end);
	}
	double weights[parent_count];
	for (unsigned int i = 0; i < parent_count; ++i) {
		weights[i] = parents[i].fitness;
	}
	std::discrete_distribution<int> parent_dist(weights, weights + parent_count);
	std::normal_distribution<double> normal_distrib(0.0, 1.0);

	population result;
	for (unsigned int i = 0; i < offspring_count; ++i) {
		organism parent1 = parents[parent_dist(population::random_engine)];
		organism parent2 = parents[parent_dist(population::random_engine)];
		int rand_2bit = population::random_engine() % 4;
		double x1, x2, sigma1, sigma2;
		// Select
		if (rand_2bit & 0b01) {
			x1 = parent1.x1;
			sigma1 = parent1.sigma1;
		}
		else {
			x1 = parent2.x1;
			sigma1 = parent2.sigma1;
		}
		if (rand_2bit & 0b01) {
			x2 = parent1.x2;
			sigma2 = parent1.sigma2;
		}
		else {
			x2 = parent2.x2;
			sigma2 = parent2.sigma2;
		}

		// Mutate
		// sigma’i = sigmai • exp(tau’ • N(0,1) + tau • Ni (0,1))
		// x’i = xi + sigma’i • Ni (0,1)
		double n1 = normal_distrib(population::random_engine);
		double n2 = normal_distrib(population::random_engine);
		sigma1 = sigma1
		         * std::exp(organism::coord_learn_rate
		                      * normal_distrib(population::random_engine)
		                    + organism::global_learn_rate * n1);
		sigma2 = sigma2
		         * std::exp(organism::coord_learn_rate
		                      * normal_distrib(population::random_engine)
		                    + organism::global_learn_rate * n2);
		x1 = x1 + sigma1 * n1;
		x2 = x2 + sigma2 * n2;

		result.insert(organism(x1, x2, sigma1, sigma2));
	}
	return result;
}

const organism &population::best() const {
	if (this->size() == 0) {
		throw std::out_of_range("cannot get best from empty population");
	}
	return *(this->rbegin());
}
