#include "population.hpp"
#include <getopt.h>
#include <iostream>

struct args_t {
	/**
	 * @brief Seed for population::seed_generator()
	 */
	uint_fast32_t seed = 1UL;
	/**
	 * @brief Size of the population that the parents are selected from
	 */
	unsigned int pop_size = 27;
	/**
	 * @brief Number of parents to use in each round (top from population)
	 */
	unsigned int parent_count = 3;
	/**
	 * @brief Maximum number of organisms to produce in evolution before stopping
	 */
	unsigned int max_organisms = 10000;
	/**
	 * @brief Maximum number of organisms to produce in evolution before stopping
	 */
	unsigned int max_generations = 10000;
	/**
	 * @brief Maximum number of organisms to produce in evolution before stopping
	 */
	int quiet = 0;
};

enum arg_type { SEED = 257, PARENTS, POP_SIZE, MAX_ORG, MAX_GEN, HELP, QUIET };

void read_args(int argc, char *const *argv, struct args_t *args_out) {
	struct option long_opts[] = {
	  {"seed", optional_argument, NULL, arg_type::SEED},
	  {"parents", optional_argument, NULL, arg_type::PARENTS},
	  {"pop_size", optional_argument, NULL, arg_type::POP_SIZE},
	  {"max_organisms", optional_argument, NULL, arg_type::MAX_ORG},
	  {"max_generations", optional_argument, NULL, arg_type::MAX_GEN},
	  {"help", optional_argument, NULL, arg_type::HELP},
	  {"quiet", optional_argument, NULL, arg_type::QUIET},
	  {0, 0, 0, 0}};
	int opt;
	while ((opt = getopt_long(argc, argv, "s:r:p:o:g:hq", long_opts, NULL))
	       != -1) {
		switch (opt) {
			case 's':
			case arg_type::SEED:
				args_out->seed = atol(optarg);
				break;
			case 'r':
			case arg_type::PARENTS:
				args_out->parent_count = atol(optarg);
				break;
			case 'p':
			case arg_type::POP_SIZE:
				args_out->pop_size = atol(optarg);
				break;
			case 'o':
			case arg_type::MAX_ORG:
				args_out->max_organisms = atol(optarg);
				break;
			case 'g':
			case arg_type::MAX_GEN:
				args_out->max_generations = atol(optarg);
				break;
			case 'q':
			case arg_type::QUIET:
				args_out->quiet += 1;
				break;

			case 'h':
			case '?':
			case arg_type::HELP:
				// print help and exit
				/* clang-format off */
				std::cerr <<
				"Usage: " << argv[0] << " [option]\n"
				"  -s\n"
				"  --seed\n"
				"          Seed for the random number generator.\n"
				"          Default 1.\n"
				"  -r\n"
				"  --parents\n"
				"          Number of parents to use in each generation.\n"
				"          Default 3.\n"
				"  -p\n"
				"  --pop_size\n"
				"          Number of organisms to create as the population the\n"
				"          parents are selected from each generation.\n"
				"          Default 27.\n"
				"  -o\n"
				"  --max_organisms\n"
				"          Maximum number of organisms to create across all\n"
				"          generations before assuming we found the best candidate.\n"
				"          Default 10,000.\n"
				"  -g\n"
				"  --max_generations\n"
				"          Maximum number of generations before assuming we found the\n"
				"          best candidate.\n"
				"          Default 10,000.\n"
				"  -h\n"
				"  --help\n"
				"          Display this help and exit.\n"
				"  -q\n"
				"  --quiet\n"
				"          Do not print for each generation.\n"
				<< std::endl;
				/* clang-format on */
				exit(opt == '?' ? 1 : 0);
				break;

			default:
				// Unreachable
				break;
		}
	}
}

int main(int argc, char *const *argv) {
	struct args_t args;
	read_args(argc, argv, &args);
	population::seed_generator(args.seed);

	unsigned int gen = 0;
	unsigned int count = args.pop_size;
	population pop(args.pop_size);
	for (; count < args.max_organisms && gen < args.max_generations;
	     count += args.pop_size, ++gen) {
		if (args.quiet < 1) {
			std::cout << "generation " << gen << ": " << pop.best() << std::endl;
		}
		pop = pop.breed(args.parent_count);
	}
	if (args.quiet < 2) {
		std::cout << "At generation " << gen << " and " << count
		          << " organisms, the best is: ";
	}
	std::cout << pop.best() << std::endl;
	return 0;
}
