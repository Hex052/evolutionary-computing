#ifndef ORGANISM_H
#define ORGANISM_H

class organism;

#include "population.hpp"
#include <compare>
#include <ostream>
#include <string>


class organism {
	friend class population;

private:
	organism() = delete;
	organism(const double x1, const double x2, const double sigma1,
	         const double sigma2);

public:
	// organism(organism &) = default;
	// organism(organism &&) = default;
	constexpr static unsigned int dimensions = 2;
	constexpr static double x1_min = -3;
	constexpr static double x1_max = 12;
	constexpr static double x2_min = 4;
	constexpr static double x2_max = 6;
	static double global_learn_rate;
	static double coord_learn_rate;
	virtual ~organism();

	// constexpr double fitness();
	// constexpr double fitness() const;
	// constexpr double cfitness() const;
	void clamp();
	void eval_fitness();

	double fitness;
	double x1;
	double x2;
	double sigma1;
	double sigma2;

	std::partial_ordering operator<=>(const organism &rhs) const;

	operator std::string() const;
};

std::ostream &operator<<(std::ostream &lhs, const organism &rhs);

#endif
