# this file is for CXX
CPPFLAGS=-iquote=$(abspath ./)
CXXFLAGS=-Wall -std=gnu++20
CFLAGS=-Wall -std=gnu17
LDLIBS=-lm #-lgmp -lgmpxx

CXX=g++-10
C=gcc-10

BIN=hw02
GENERAL=$(realpath ../general/)
OUTPUTDIR=$(abspath ./out/)
RELEASE_DIR=$(OUTPUTDIR)/release/
DEBUG_DIR=$(OUTPUTDIR)/debug/

.PHONY: default
default: debug

.PHONY: release
release: CPPFLAGS+=-DNDEBUG -DRELEASE -g0
release: CXXFLAGS+=-O2
release: CFLAGS+=-O2
release: $(RELEASE_DIR)/$(BIN)

.PHONY: debug
debug: CPPFLAGS+=-DDEBUG -D_GLIBCXX_DEBUG=1 -D_CLIBCXX_DEBUG=1 -g3 -ggdb3
debug: CXXFLAGS+=-O0
debug: CFLAGS+=-O0
debug: $(DEBUG_DIR)/$(BIN)

# List of all source files.
CPP_SOURCE=$(wildcard *.cpp)
# List of all object files
OBJ_RELEASE=$(CPP_SOURCE:%.cpp=$(RELEASE_DIR)/obj/%.o)
OBJ_DEBUG=$(CPP_SOURCE:%.cpp=$(DEBUG_DIR)/obj/%.o)
# List of all dependencies
DEP=$(CPP_SOURCE:%.cpp=$(DEBUG_DIR)/obj/%.d) $(CPP_SOURCE:%.cpp=$(RELEASE_DIR)/obj/%.d)


$(RELEASE_DIR)/$(BIN): $(OBJ_RELEASE) | $(RELEASE_DIR)
$(DEBUG_DIR)/$(BIN): $(OBJ_DEBUG) | $(DEBUG_DIR)
$(RELEASE_DIR)/$(BIN) $(DEBUG_DIR)/$(BIN):
	$(CXX) $(CXXFLAGS) -o $@ $^ $(LDLIBS)

-include $(DEP)

$(DEBUG_DIR)/obj/*.o: | $(DEBUG_DIR)/obj/
$(RELEASE_DIR)/obj/*.o: | $(RELEASE_DIR)/obj/
$(RELEASE_DIR)/obj/%.o $(DEBUG_DIR)/obj/%.o : %.cpp
	$(CXX) -c $(CPPFLAGS) $(CXXFLAGS) -MMD -o $@ $(abspath $<)
# The -MMD flags additionaly creates a .d file with
#  the same name as the .o file.
# The .d file contains recipies to create the .o file.

$(OBJ_DEBUG): | $(DEBUG_DIR)/obj/
$(OBJ_RELEASE): | $(RELEASE_DIR)/obj/
$(DEBUG_DIR)/obj/: | $(DEBUG_DIR)
$(RELEASE_DIR)/obj/: | $(RELEASE_DIR)
$(RELEASE_DIR): | $(OUTPUTDIR)
$(DEBUG_DIR): | $(OUTPUTDIR)

$(DEBUG_DIR) $(RELEASE_DIR) $(OUTPUTDIR) $(RELEASE_DIR)/obj/ $(DEBUG_DIR)/obj/:
	mkdir $@

.PHONY: tar
tar: $(OUTPUTDIR)/$(BIN).tar.gz


$(OUTPUTDIR)/$(BIN).tar.gz: $(OUTPUTDIR)/$(BIN).tar | $(OUTPUTDIR)
	gzip < $< > $@

$(OUTPUTDIR)/$(BIN).tar: $(RELEASE_DIR)/$(BIN)
# Most normal tarballs would not include $(RELEASE_DIR)/$(BIN), but
#  I need to turn in the final executable too.
$(OUTPUTDIR)/$(BIN).tar: $(CPP_SOURCE) $(wildcard *.hpp)
$(OUTPUTDIR)/$(BIN).tar: Makefile test report.md
$(OUTPUTDIR)/$(BIN).tar: | $(OUTPUTDIR)
	@# cd .. && tar -az $(abspath $@) $(abspath $?)
	tar -uf $(abspath $@) -- $?


.PHONY: clean
clean:
	$(RM) -r $(OUTPUTDIR)
.PHONY: clean-obj
clean-obj:
	$(RM) -r $(OBJ_RELEASE) $(OBJ_DEBUG)
.PHONY: clean-tar
clean-tar:
	$(RM) -r $(OUTPUTDIR)/$(BIN).tar.gz
