# CSCE A412: Homework 02

This implementation work fine, it is near the global optimum at
`x = 11.625` and `y = 5.925` (fitness of 39.05).
Lowering the learning rate improved the results over the original, though the occational duds still did manage to creep in.
If you want to run random seeds each time, there are some commented out lines
in `test` that will let you use random seeds each time, though this isn't
POSIX-complaint so you'll need `bash` or maybe `zsh`, though I've not tried the
latter.

I didn't place any bounds on what the normal distribution produced, which
sometimes caused results such as:
```c
{ .x1=10.8126, .x2=6.0000, .sigma1=0.0000, .sigma2=1088589.0506, .fitness=32.3126 }
```
I suspect this is from the normal distribution producing a value far from zero occationally.
However, this did not seem to have an impact on the quality of the results
produced, as the results are very near the global optimum regardless.
In other situations it may have more of an impact, but I did not think it was
worth correcting given it was not causing a problem.

The results from one run of `test` were as follows. Note: if your stdlib uses
a different implementation of the random number generators, then your results
will likely be different from mine.
```c
./out/release/hw02 -qqs 172627500
	{ .x1=11.8126, .x2=5.5250, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.8376 }
./out/release/hw02 -qqs 268821895
	{ .x1=12.0000, .x2=5.0251, .sigma1=7.2315, .sigma2=0.0000, .fitness=26.5250 }
./out/release/hw02 -qqs 393119789
	{ .x1=11.8126, .x2=5.6250, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.9376 }
./out/release/hw02 -qqs 29486470
	{ .x1=10.5626, .x2=5.8250, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.8876 }
./out/release/hw02 -qqs 3092928445
	{ .x1=7.3127, .x2=5.2250, .sigma1=0.0000, .sigma2=0.0000, .fitness=34.0376 }
./out/release/hw02 -qqs 267046425
	{ .x1=9.5627, .x2=4.0251, .sigma1=0.0000, .sigma2=0.0000, .fitness=35.0876 }
./out/release/hw02 -qqs 21305932
	{ .x1=11.0624, .x2=5.4250, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.9874 }
./out/release/hw02 -qqs 1328019591
	{ .x1=10.3127, .x2=5.8250, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.6376 }
./out/release/hw02 -qqs 259568830
	{ .x1=11.3126, .x2=4.7251, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.5376 }
./out/release/hw02 -qqs 605325854
	{ .x1=10.3166, .x2=5.4250, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.1868 }
./out/release/hw02 -qqs 47308534
	{ .x1=11.3126, .x2=5.0251, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.8376 }
./out/release/hw02 -qqs 87486714
	{ .x1=10.8126, .x2=4.8251, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.1376 }
./out/release/hw02 -qqs 55031503
	{ .x1=6.5622, .x2=4.0251, .sigma1=0.0000, .sigma2=0.0000, .fitness=32.0870 }
./out/release/hw02 -qqs 92926594
	{ .x1=11.5626, .x2=5.8250, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.8876 }
./out/release/hw02 -qqs 1828728847
	{ .x1=11.8126, .x2=5.1260, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.4275 }
./out/release/hw02 -qqs 1124325958
	{ .x1=10.8126, .x2=6.0000, .sigma1=0.0000, .sigma2=1088589.0506, .fitness=32.3126 }
./out/release/hw02 -qqs 425614718
	{ .x1=11.8126, .x2=4.7251, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.0376 }
./out/release/hw02 -qqs 12504583
	{ .x1=11.8126, .x2=4.8251, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.1376 }
./out/release/hw02 -qqs 2262720969
	{ .x1=10.9307, .x2=5.1250, .sigma1=0.0018, .sigma2=0.0000, .fitness=15.8554 }
./out/release/hw02 -qqs 950318184
	{ .x1=8.3127, .x2=4.7251, .sigma1=0.0000, .sigma2=0.0000, .fitness=34.5376 }
./out/release/hw02 -qqs 292535418
	{ .x1=11.0626, .x2=5.5250, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.0876 }
./out/release/hw02 -qqs 9836358
	{ .x1=3.5591, .x2=5.9250, .sigma1=0.0039, .sigma2=0.0000, .fitness=30.9708 }
./out/release/hw02 -qqs 68986584
	{ .x1=11.3128, .x2=5.2251, .sigma1=0.0001, .sigma2=0.0000, .fitness=38.0375 }
./out/release/hw02 -qqs 1193812096
	{ .x1=9.5627, .x2=5.1471, .sigma1=0.0000, .sigma2=0.0004, .fitness=32.0004 }
./out/release/hw02 -qqs 2006812344
	{ .x1=10.3212, .x2=4.9253, .sigma1=0.0003, .sigma2=0.0002, .fitness=36.5005 }
./out/release/hw02 -qqs 2806610686
	{ .x1=8.8127, .x2=4.3251, .sigma1=0.0000, .sigma2=0.0000, .fitness=34.6376 }
./out/release/hw02 -qqs 15712687
	{ .x1=11.0626, .x2=6.0000, .sigma1=0.0000, .sigma2=7441.4223, .fitness=32.5626 }
./out/release/hw02 -qqs 212022653
	{ .x1=10.0627, .x2=5.4448, .sigma1=0.0000, .sigma2=0.0019, .fitness=33.3262 }
./out/release/hw02 -qqs 2468318999
	{ .x1=11.0626, .x2=5.5250, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.0876 }
./out/release/hw02 -qqs 266772006
	{ .x1=10.5627, .x2=4.8251, .sigma1=0.0000, .sigma2=0.0000, .fitness=36.8876 }
./out/release/hw02 -qqs 55788463
	{ .x1=7.5627, .x2=5.4976, .sigma1=0.0000, .sigma2=0.0107, .fitness=28.2230 }
./out/release/hw02 -qqs 135221788
	{ .x1=11.3126, .x2=4.8250, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.6376 }
./out/release/hw02 -qqs 150363445
	{ .x1=11.8126, .x2=5.7251, .sigma1=0.0000, .sigma2=0.0000, .fitness=39.0376 }
./out/release/hw02 -qqs 55213892
	{ .x1=10.5626, .x2=4.0251, .sigma1=0.0000, .sigma2=0.0000, .fitness=36.0876 }
./out/release/hw02 -qqs 1199913627
	{ .x1=10.5326, .x2=4.7251, .sigma1=0.0003, .sigma2=0.0000, .fitness=33.9294 }
./out/release/hw02 -qqs 239572842
	{ .x1=11.8126, .x2=4.7251, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.0376 }
./out/release/hw02 -qqs 114312864
	{ .x1=11.5626, .x2=6.0000, .sigma1=0.0000, .sigma2=1.4173, .fitness=33.0626 }
./out/release/hw02 -qqs 113273628
	{ .x1=12.0000, .x2=4.4040, .sigma1=7.5032, .sigma2=0.4362, .fitness=22.5981 }
./out/release/hw02 -qqs 68944169
	{ .x1=10.5626, .x2=5.0251, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.0876 }
./out/release/hw02 -qqs 191965261
	{ .x1=9.3127, .x2=4.2251, .sigma1=0.0000, .sigma2=0.0000, .fitness=35.0376 }
./out/release/hw02 -qqs 274887813
	{ .x1=11.5636, .x2=5.0537, .sigma1=0.0004, .sigma2=0.0436, .fitness=31.8983 }
./out/release/hw02 -qqs 181756436
	{ .x1=11.0626, .x2=5.7250, .sigma1=0.0000, .sigma2=0.0000, .fitness=38.2876 }
./out/release/hw02 -qqs 1104632412
	{ .x1=12.0000, .x2=4.8251, .sigma1=0.3034, .sigma2=0.0000, .fitness=26.3250 }
./out/release/hw02 -qqs 2223527410
	{ .x1=6.8127, .x2=5.0251, .sigma1=0.0000, .sigma2=0.0000, .fitness=33.3376 }
./out/release/hw02 -qqs 372522211
	{ .x1=9.8127, .x2=5.3250, .sigma1=0.0000, .sigma2=0.0000, .fitness=36.6376 }
./out/release/hw02 -qqs 1472721383
	{ .x1=8.0626, .x2=5.7250, .sigma1=0.0000, .sigma2=0.0000, .fitness=35.2876 }
./out/release/hw02 -qqs 2255127291
	{ .x1=11.3169, .x2=5.2252, .sigma1=0.0000, .sigma2=0.0000, .fitness=37.9725 }
./out/release/hw02 -qqs 979910500
	{ .x1=10.3127, .x2=5.1208, .sigma1=0.0000, .sigma2=0.0005, .fitness=36.7601 }
./out/release/hw02 -qqs 2202711514
	{ .x1=8.3127, .x2=5.6251, .sigma1=0.0000, .sigma2=0.0000, .fitness=35.4376 }
./out/release/hw02 -qqs 404816763
	{ .x1=11.8126, .x2=5.7250, .sigma1=0.0000, .sigma2=0.0000, .fitness=39.0376 }
```

## Build Instructions

Requres a compiler that can process C++20, such as `g++-10`.

### Steps:

- `make release`
- Executable now at `out/release/hw02`

`make debug` will compile to the `out/debug/` folder, and all binaries will be
compiled with the appropriate debug flags and no optimizations.
`make release` will compile with optimizations.
