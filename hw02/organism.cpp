#include "organism.hpp"
#include <cmath>
#include <iomanip>
#include <sstream>

double organism::global_learn_rate =
  1.0 / std::sqrt(2 * organism::dimensions) / 8;
double organism::coord_learn_rate =
  1.0 / std::sqrt(2 * std::sqrt(organism::dimensions));

organism::organism(const double px1, const double px2, const double psigma1,
                   const double psigma2)
    : x1(px1), x2(px2), sigma1(psigma1), sigma2(psigma2) {
	this->clamp();
	this->eval_fitness();
}
organism::~organism() {
}

// double organism::fitness() {
// 	this->eval_fitness();
// 	return this->_fitness;
// }
// double organism::fitness() const {
// 	return this->_fitness;
// }
// double organism::cfitness() const {
// 	return this->_fitness;
// }
void organism::eval_fitness() {
	this->fitness = 21.5 + (this->x1 * std::sin(8 * M_PI * this->x1))
	                + (this->x2 * std::sin(20 * M_PI * this->x2));
}
void organism::clamp() {
	if (this->x1 > organism::x1_max) {
		this->x1 = organism::x1_max;
	}
	else if (this->x1 < organism::x1_min) {
		this->x1 = organism::x1_min;
	}
	if (this->x2 > organism::x2_max) {
		this->x2 = organism::x2_max;
	}
	else if (this->x2 < organism::x2_min) {
		this->x2 = organism::x2_min;
	}
	// sigma
	if (this->sigma1 < 0) {
		this->sigma1 = 0;
	}
	if (this->sigma2 < 0) {
		this->sigma2 = 0;
	}
}


std::partial_ordering organism::operator<=>(const organism &rhs) const {
	return this->fitness <=> rhs.fitness;
}


organism::operator std::string() const {
	std::stringstream str;
	str << std::setprecision(4) << std::fixed;
	str << "{ .x1=" << this->x1 << ", .x2=" << this->x2;
	str << ", .sigma1=" << this->sigma1 << ", .sigma2=" << this->sigma2;
	str << ", .fitness=" << this->fitness << " }";
	return str.str();
}

std::ostream &operator<<(std::ostream &lhs, const organism &rhs) {
	return lhs << std::string(rhs);
}
