#ifndef POPULATION_H
#define POPULATION_H

class population;

#include "organism.hpp"
#include <random>
#include <set>
#include <stdexcept>


class population : public std::set<organism> {
private:
	static std::default_random_engine random_engine;
	population();

public:
	population(unsigned int initial_size);
	// using fitness_iterator = fitness_iterator;
	static void seed_generator(uint_fast32_t seed);

	virtual ~population();

	/**
	 * @brief Breed a new population from the top @p parent_count
	 *
	 * @param parent_count mu number of parents
	 * @return population new population of the same size as the current one
	 */
	population breed(unsigned int parent_count) const;
	/**
	 * @brief Breed a new population from the top @p parent_count
	 *
	 * @param parent_count mu number of parents
	 * @param offspring_count lambda number of offspring
	 * @return population new population of size @p offspring_count
	 */
	population breed(unsigned int parent_count,
	                 unsigned int offspring_count) const;

	/**
	 * @brief Gets the best member or throws std::out_of_range if empty
	 *
	 * @return const organism& The organism with the highest fitness value
	 * @throw std::out_of_range No organisms in population
	 */
	const organism &best() const;

	// set<organism>::iterator begin() noexcept;
	// set<organism>::iterator begin() const noexcept;
	// set<organism>::iterator cbegin() const noexcept;
	// set<organism>::iterator end() noexcept;
	// set<organism>::iterator end() const noexcept;
	// set<organism>::iterator cend() const noexcept;
	// set<organism>::iterator rbegin() noexcept;
	// set<organism>::iterator rbegin() const noexcept;
	// set<organism>::iterator crbegin() const noexcept;
	// set<organism>::iterator rend() noexcept;
	// set<organism>::iterator rend() const noexcept;
	// set<organism>::iterator crend() const noexcept;

	// fitness_iterator fbegin() const noexcept;
	// fitness_iterator cfbegin() const noexcept;
	// fitness_iterator fend() const noexcept;
	// fitness_iterator cfend() const noexcept;
	// std::reverse_iterator<fitness_iterator> rfbegin() const noexcept;
	// std::reverse_iterator<fitness_iterator> crfbegin() const noexcept;
	// std::reverse_iterator<fitness_iterator> rfend() const noexcept;
	// std::reverse_iterator<fitness_iterator> crfend() const noexcept;
};

// class fitness_iterator {
// private:
// 	std::set<organism>::iterator iter;
//
// public:
// 	using iterator_category = std::bidirectional_iterator_tag;
// 	using value_type = const double;
// 	using difference_type = std::ptrdiff_t;
// 	using pointer = const double *;
// 	using reference = const double &;
//
// 	bool operator==(fitness_iterator const &rhs) const {
// 		return this->iter == rhs.iter;
// 	}
// 	bool operator!=(fitness_iterator const &rhs) const {
// 		return this->iter == rhs.iter;
// 	}
//
// 	const double &operator*() const {
// 		return this->iter->fitness;
// 	}
// 	const double *operator->() const {
// 		return &(this->iter->fitness);
// 	}
//
// 	fitness_iterator &operator++() {
// 		++(this->iter);
// 		return *this;
// 	}
// 	fitness_iterator operator++(int) {
// 		fitness_iterator temp(*this);
// 		++(this->iter);
// 		return temp;
// 	}
// 	fitness_iterator &operator--() {
// 		--(this->iter);
// 		return *this;
// 	}
// 	fitness_iterator operator--(int) {
// 		fitness_iterator temp(*this);
// 		--(this->iter);
// 		return temp;
// 	}
// };

#endif
